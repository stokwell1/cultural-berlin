FROM ruby:3.1.1-alpine AS builder

# Nokogiri's build dependencies
RUN apk add --update \
  build-base \
  libxml2-dev \
  libxslt-dev 
# Nokogiri, yikes
RUN echo 'source "https://rubygems.org"; gem "nokogiri"' > Gemfile

RUN gem install bundler -v 2.3.8

RUN bundle install

# The final image: we start clean
FROM ruby:3.1.1-alpine AS cultural-berlin-app

# We copy over the entire gems directory for our builder image, containing the already built artifact
COPY --from=builder /usr/local/bundle/ /usr/local/bundle/

RUN apk update \
  && apk upgrade \
  && apk add --update --no-cache \
  build-base \
  curl-dev \
  git \
  postgresql-dev \
  yaml-dev \
  zlib-dev \
  nodejs \
  yarn \
  bash

RUN gem update --system

# use a global path instead of vendor
ENV GEM_HOME="/usr/local/bundle"
ENV BUNDLE_PATH="$GEM_HOME"
ENV BUNDLE_SILENCE_ROOT_WARNING=1
ENV BUNDLE_APP_CONFIG="$GEM_HOME"
ENV PATH="$GEM_HOME/bin:$BUNDLE_PATH/gems/bin:${PATH}"

# copy the source
WORKDIR /app

COPY . /app


RUN chmod +x ./entrypoints/*

RUN gem install bundler -v 2.3.8

RUN bundle install --jobs=8

RUN yarn install
