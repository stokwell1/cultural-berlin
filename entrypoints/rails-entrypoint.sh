#!/bin/sh

set -e

if [ -f tmp/pids/server.pid ]; then
 rm tmp/pids/server.pid
fi

bundle exec rake assets:precompile
bundle exec rails db:create
bundle exec rails db:migrate

case $1 in

  web)
    exec bundle exec puma -C config/puma.rb
  ;;

  *)
    exec "$@"
  ;;

esac

exit 0